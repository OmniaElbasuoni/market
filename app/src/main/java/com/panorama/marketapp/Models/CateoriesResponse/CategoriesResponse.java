package com.panorama.marketapp.Models.CateoriesResponse;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class CategoriesResponse implements Parcelable {

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;


	@SerializedName("category_img")
	private String categoryImg;

	@SerializedName("products")
	private ArrayList<ProductsItem> products;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setCategoryImg(String categoryImg){
		this.categoryImg = categoryImg;
	}

	public String getCategoryImg(){
		return categoryImg;
	}

	public void setProducts(ArrayList<ProductsItem> products){
		this.products = products;
	}

	public ArrayList<ProductsItem> getProducts(){
		return products;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.name);
		dest.writeInt(this.id);
		dest.writeTypedList(this.products);
		dest.writeString(this.categoryImg);
	}

	public CategoriesResponse() {
	}

	protected CategoriesResponse(Parcel in) {
		this.name = in.readString();
		this.id = in.readInt();
		this.products = in.createTypedArrayList(ProductsItem.CREATOR);
		this.categoryImg = in.readString();
	}

	public static final Parcelable.Creator<CategoriesResponse> CREATOR = new Parcelable.Creator<CategoriesResponse>() {
		@Override
		public CategoriesResponse createFromParcel(Parcel source) {
			return new CategoriesResponse(source);
		}

		@Override
		public CategoriesResponse[] newArray(int size) {
			return new CategoriesResponse[size];
		}
	};
}