package com.panorama.marketapp.Models.CateoriesResponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProductsItem implements Parcelable {

	@SerializedName("price")
	private String price;

	@SerializedName("name")
	private String name;

	@SerializedName("weight")
	private String weight;

	@SerializedName("id")
	private String id;

	@SerializedName("product_img")
	private String productImg;

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setWeight(String weight){
		this.weight = weight;
	}

	public String getWeight(){
		return weight;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setProductImg(String productImg){
		this.productImg = productImg;
	}

	public String getProductImg(){
		return productImg;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.price);
		dest.writeString(this.name);
		dest.writeString(this.weight);
		dest.writeString(this.id);
		dest.writeString(this.productImg);
	}

	public ProductsItem() {
	}

	protected ProductsItem(Parcel in) {
		this.price = in.readString();
		this.name = in.readString();
		this.weight = in.readString();
		this.id = in.readString();
		this.productImg = in.readString();
	}

	public static final Parcelable.Creator<ProductsItem> CREATOR = new Parcelable.Creator<ProductsItem>() {
		@Override
		public ProductsItem createFromParcel(Parcel source) {
			return new ProductsItem(source);
		}

		@Override
		public ProductsItem[] newArray(int size) {
			return new ProductsItem[size];
		}
	};
}