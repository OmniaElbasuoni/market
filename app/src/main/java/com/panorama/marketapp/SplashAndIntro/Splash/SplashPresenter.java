package com.panorama.marketapp.SplashAndIntro.Splash;

import android.content.SharedPreferences;
import android.content.res.Configuration;

import com.panorama.marketapp.Utils.LanguageType;

import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class SplashPresenter {
    private SplashInterface view;
    private String type = "", language = "";

    SplashPresenter(SplashInterface view) {
        this.view = view;
    }

    void ReadSharedPreference() {
        SharedPreferences prefs = view.getContxt().getSharedPreferences(view.getContxt().getPackageName(), MODE_PRIVATE);
        type = prefs.getString("type", "english");
        language = prefs.getString("language", "en");

        setLanguages();

        view.loading();
    }

    private void setLanguages() {
        LanguageType languageType = new LanguageType();
        languageType.languageType = type;
        Configuration config = new Configuration();
        config.locale = new Locale(language);
        view.getContxt().getResources().updateConfiguration(config,view.getContxt().getResources().getDisplayMetrics());
    }

}
