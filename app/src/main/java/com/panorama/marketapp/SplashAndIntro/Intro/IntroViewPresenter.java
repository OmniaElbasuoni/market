package com.panorama.marketapp.SplashAndIntro.Intro;

import androidx.viewpager.widget.ViewPager;

public interface IntroViewPresenter {
    void setupViewPager(ViewPager viewPager);
    void loading();
    void onViewPagerChange();
    void getIntroData();
    void setSwiper();
}
