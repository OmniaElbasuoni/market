package com.panorama.marketapp.SplashAndIntro.Intro;

import android.os.Bundle;
import android.os.Handler;

import androidx.viewpager.widget.ViewPager;

import com.panorama.marketapp.SplashAndIntro.Intro.Adapter.ViewPagerAdapter;
import com.panorama.marketapp.SplashAndIntro.Intro.FirstIntro.FirstFragment;
import com.panorama.marketapp.SplashAndIntro.Intro.SecondIntro.SecondFragment;
import com.panorama.marketapp.SplashAndIntro.Intro.ThirdIntro.ThirdFragment;


public class IntroPresenter implements IntroViewPresenter{

    private IntroFragment context ;
    private ViewPagerAdapter adapter ;
    private int pos = 0 ;
    private boolean isFinish = true;


     IntroPresenter(IntroFragment context) {
        this.context = context;
    }

    @Override
    public void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter((context).getChildFragmentManager());

        Bundle bundle = new Bundle();
        //bundle.putSerializable("intro" , intro);
        FirstFragment firstFragment = new FirstFragment();
        SecondFragment secondFragment = new SecondFragment();
        ThirdFragment thirdFragment = new ThirdFragment();
        firstFragment.setArguments(bundle);
        secondFragment.setArguments(bundle);
        thirdFragment.setArguments(bundle);
        adapter.addFragment(firstFragment, "first");
        adapter.addFragment(secondFragment, "second");
        adapter.addFragment(thirdFragment, "third");

        viewPager.setAdapter(adapter);
        context.pageIndicatorView.setViewPager(viewPager);
    }

    @Override
    public void loading(){
        Handler friend = new Handler();
        friend.postDelayed(() -> {
            if (pos!=adapter.getCount()){
                context.introViewPager.setCurrentItem(pos , true);
                pos++;
            }else {
                if(isFinish){
                    isFinish=false;
                    context.goToHome();
                }
                /*Intent intent = new Intent(context.getContext() , AuthContainerActivity.class);
                context.startActivity(intent);
                context.finish();*/
            }
            if (isFinish)
                loading();
        }, 2000);
    }

    @Override
    public void onViewPagerChange(){
        context.introViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }
            @Override
            public void onPageSelected(int i) {
                pos=i;
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @Override
    public void getIntroData() {
        //ParentClass.showWaiting(context);
        setupViewPager(context.introViewPager /*, response.body().getData()*/);
        loading();
        /*ApiUtils.getSharedNetwork().getIntroData(ParentClass.getLocalization(context)).enqueue(new Callback<IntroResponse>() {
            @Override
            public void onResponse(Call<IntroResponse> call, Response<IntroResponse> response) {
                ParentClass.dismissDialog();
                if (response.isSuccessful()){
                    if (response.body().isValue()){
                        setupViewPager(context.introViewPager , response.body().getData());
                        loading();
                    }else {
                        Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<IntroResponse> call, Throwable t) {
                ParentClass.dismissDialog();
                ParentClass.handleException(context,t);
            }
        });*/
    }

    @Override
    public void setSwiper() {
        if (context.introViewPager.getCurrentItem() == adapter.getCount()-1){
            isFinish = false;
            context.goToHome();
            /*Intent intent = new Intent(context , AuthContainerActivity.class) ;
            context.startActivity(intent);
            context.finishAffinity();*/
        }else
            context.introViewPager.setCurrentItem(context.introViewPager.getCurrentItem()+1 , true);
    }


}
