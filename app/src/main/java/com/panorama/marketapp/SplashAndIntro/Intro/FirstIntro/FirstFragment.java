package com.panorama.marketapp.SplashAndIntro.Intro.FirstIntro;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.panorama.marketapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class FirstFragment extends Fragment {



    public FirstFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_frist, container, false);
        ButterKnife.bind(this,view);
        //Data intro = (Data)getArguments().get("intro");
        return view;
    }

}
