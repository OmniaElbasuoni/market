package com.panorama.marketapp.SplashAndIntro.Splash;

import android.content.Context;

public interface SplashInterface {
    void loading();
    Context getContxt();
    void openMainActivity();
}
