package com.panorama.marketapp.SplashAndIntro.Intro;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.viewpager.widget.ViewPager;

import com.panorama.marketapp.R;
import com.panorama.marketapp.Utils.SharedPrefManager;
import com.rd.PageIndicatorView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class IntroFragment extends Fragment implements IntroView{

    @BindView(R.id.introViewPager)
    ViewPager introViewPager;
    @BindView(R.id.Next)
    Button Next;
    @BindView(R.id.pageIndicatorView)
    PageIndicatorView pageIndicatorView;
    private NavController navController;

    private IntroPresenter mIntroPresenter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_intro,container,false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
        mIntroPresenter = new IntroPresenter(this);
        mIntroPresenter.getIntroData();
        mIntroPresenter.onViewPagerChange();
    }

    @OnClick(R.id.Next)
    public void onViewClicked() {
        mIntroPresenter.setSwiper();
    }


    @Override
    public void goToHome() {
        navController.navigate(R.id.action_introFragment_to_homeContainerFragment);
    }
}
