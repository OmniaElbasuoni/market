package com.panorama.marketapp.SplashAndIntro.Intro.ThirdIntro;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.panorama.marketapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ThirdFragment extends Fragment {




    public ThirdFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_third, container, false);
        ButterKnife.bind(this,view);
        //Data intro = (Data)getArguments().get("intro");
        //txtFeatureC.setText(intro.getAdv3());
        return view;
    }

}
