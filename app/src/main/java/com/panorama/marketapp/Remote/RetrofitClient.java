package com.panorama.marketapp.Remote;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static Retrofit retrofit=null;

    static Retrofit getClient() {

        if (retrofit==null)
        {
            retrofit=new Retrofit.Builder()
                    .baseUrl("https://5bcce576cf2e850013874767.mockapi.io/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(setTime())
                    .build();
        }
        return retrofit;
    }

    public static void setRetrofit(Retrofit retrofit) {
        RetrofitClient.retrofit = retrofit;
    }

    private static OkHttpClient setTime() {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();

        return  okHttpClient;
    }
}
