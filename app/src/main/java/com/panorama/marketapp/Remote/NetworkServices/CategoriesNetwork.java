package com.panorama.marketapp.Remote.NetworkServices;

import com.panorama.marketapp.Models.CateoriesResponse.CategoriesResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CategoriesNetwork {

    @GET("task/categories")
    Call<List<CategoriesResponse>> getCategories();
}
