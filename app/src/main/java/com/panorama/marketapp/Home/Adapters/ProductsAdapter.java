package com.panorama.marketapp.Home.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.recyclerview.widget.RecyclerView;

import com.panorama.marketapp.Models.CateoriesResponse.CategoriesResponse;
import com.panorama.marketapp.Models.CateoriesResponse.ProductsItem;
import com.panorama.marketapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.productsListViewHolder>{


    private List<ProductsItem> productsItemList;
    private Context context;
    NavController navController;

    public ProductsAdapter(List<ProductsItem> productsItemList, NavController navController) {
        this.productsItemList = productsItemList;
        this.navController = navController;
    }

    @NonNull
    @Override
    public productsListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_row,parent,false);
        return new productsListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(productsListViewHolder holder, int position) {
        holder.bind(productsItemList.get(position));
    }

    @Override
    public int getItemCount() {
        return productsItemList.size();
    }



    public class productsListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.productImage)
        ImageView productImage;
        @BindView(R.id.productName)
        TextView productName;
        @BindView(R.id.productWeight)
        TextView productWeight;
        @BindView(R.id.productPrice)
        TextView productPrice;
        @BindView(R.id.linear)
        LinearLayout linear;

        productsListViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

            linear.setOnClickListener(v -> {
                int pos=getAdapterPosition();
               Bundle bundle = new Bundle();
                bundle.putString("productName",productsItemList.get(pos).getName());
                bundle.putString("productImage",productsItemList.get(pos).getProductImg());
                navController.navigate(R.id.action_productsFragment_to_productDetailsFragment,bundle);
            });

        }


        public void bind(ProductsItem productsItem){

            productName.setText(productsItem.getName());
            productWeight.setText(productsItem.getWeight());
            productPrice.setText(productsItem.getPrice());


            //this code to prevent crash when the backend beh return empty image
            if(productsItem.getProductImg().equals("")){
                productsItem.setProductImg("544454");
            }


            Picasso.get()
                    .load(productsItem.getProductImg())
                    .placeholder(R.drawable.image_loading)
                    .error(R.drawable.no_image)
                    .into(productImage);

        }



    }

}
