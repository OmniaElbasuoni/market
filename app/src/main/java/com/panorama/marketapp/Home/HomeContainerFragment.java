package com.panorama.marketapp.Home;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.panorama.marketapp.Home.Adapters.CategoriesAdapter;
import com.panorama.marketapp.Home.Adapters.OffersSliderAdapter;
import com.panorama.marketapp.Models.CateoriesResponse.CategoriesResponse;
import com.panorama.marketapp.Models.SliderOffers;
import com.panorama.marketapp.R;
import com.shuhart.bubblepagerindicator.BubblePageIndicator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeContainerFragment extends Fragment implements HomeView{


    @BindView(R.id.viewpagerOffers)
    ViewPager viewpagerOffers;
    @BindView(R.id.indicatorOffers)
    BubblePageIndicator indicatorOffers;
    @BindView(R.id.categoriesRecHome)
    RecyclerView categoriesRecHome;

    private OffersSliderAdapter offersSliderAdapter;
    private List<SliderOffers> galleryList;

    private HomePresenter mPresenter;

    private CategoriesAdapter categoriesAdapter;
    private List<CategoriesResponse> categoriesResponseList;

    LinearLayoutManager mLinearLayoutManager;
NavController navController;


    public HomeContainerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_home_container, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
        setupUI();
        mPresenter.getOffersSlider();
        mPresenter.getCategoriesList();
    }

    private void setupUI() {

        //init presenter
        mPresenter = new HomePresenter(this);


        galleryList=new ArrayList<>();
        offersSliderAdapter = new OffersSliderAdapter(getActivity(),galleryList);
        viewpagerOffers.setAdapter(offersSliderAdapter);
        indicatorOffers.setViewPager(viewpagerOffers);


        //init  RecycleView
        categoriesResponseList =new ArrayList<>();
        mLinearLayoutManager = new GridLayoutManager(getContext(),2);
        categoriesAdapter =new CategoriesAdapter(categoriesResponseList,navController);

        // Set up the RecyclerView
        categoriesRecHome.setHasFixedSize(true);
        categoriesRecHome.setLayoutManager(mLinearLayoutManager);
        categoriesRecHome.setAdapter(categoriesAdapter);

    }


    @Override
    public void onOffersSliderResponse() {

        SliderOffers sliderOffers=new SliderOffers();

        for (int i=0 ;i <4 ;i++) {

            sliderOffers.setId(0);
            sliderOffers.setImage("");
            galleryList.add(sliderOffers);

        }
        offersSliderAdapter.notifyDataSetChanged();

    }

    @Override
    public void onProductsListResponse(List<CategoriesResponse> categoriesResponseList) {
        if(categoriesResponseList.size() > 0 ){
            categoriesAdapter.addProductList(categoriesResponseList);
        }
    }

    @Override
    public void getErrorMessage(String msg, int code, Throwable t, Boolean isToast) {

    }


}
