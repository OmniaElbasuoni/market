package com.panorama.marketapp.Home.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.navigation.NavController;
import androidx.recyclerview.widget.RecyclerView;

import com.panorama.marketapp.Models.CateoriesResponse.CategoriesResponse;
import com.panorama.marketapp.Models.CateoriesResponse.ProductsItem;
import com.panorama.marketapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoriesListViewHolder>{


    private List<CategoriesResponse> categoriesResponseList;
    private Context context;
    NavController navController;



    public CategoriesAdapter(List<CategoriesResponse> categoriesResponseList,NavController navController) {
        this.categoriesResponseList = categoriesResponseList;
        this.navController=navController;
    }

    @NonNull
    @Override
    public CategoriesListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.categories_row,parent,false);
        return new CategoriesListViewHolder(view);    }

    @Override
    public void onBindViewHolder(CategoriesListViewHolder holder, int position) {
        holder.bind(categoriesResponseList.get(position));
    }

    @Override
    public int getItemCount() {
        return categoriesResponseList.size();
    }

    public void addProductList(List<CategoriesResponse> productList){
        int startPosition=categoriesResponseList.size();
        if(categoriesResponseList.size()==0){
            categoriesResponseList.addAll(productList);
            notifyDataSetChanged();
        }
        else {
            categoriesResponseList.addAll(productList);
            notifyItemRangeInserted(startPosition,productList.size());
        }
    }

    public void clearData(){
        categoriesResponseList.clear();
        notifyDataSetChanged();
    }



    public class CategoriesListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.categoryImage)
        ImageView categoryImage;
        @BindView(R.id.categoryName)
        TextView categoryName;
        @BindView(R.id.linear)
        RelativeLayout linear;


        CategoriesListViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

            linear.setOnClickListener(v -> {
                int pos=getAdapterPosition();
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("Products",categoriesResponseList.get(pos).getProducts());
                bundle.putString("categoryName",categoriesResponseList.get(pos).getName());
                bundle.putString("categoryImage",categoriesResponseList.get(pos).getCategoryImg());
                navController.navigate(R.id.action_homeContainerFragment_to_productsFragment,bundle);
            });

        }


        public void bind(CategoriesResponse categoriesResponse){

            categoryName.setText(categoriesResponse.getName());


            //this code to prevent crash when the backend beh return empty image
            if(categoriesResponse.getCategoryImg().equals("")){
                categoriesResponse.setCategoryImg("544454");
            }


            Picasso.get()
                    .load(categoriesResponse.getCategoryImg())
                    .placeholder(R.drawable.image_loading)
                    .error(R.drawable.no_image)
                    .into(categoryImage);

        }



    }

}
