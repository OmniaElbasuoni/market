package com.panorama.marketapp.Home;

public interface HomeViewPresenter {
    void getOffersSlider();
    void getCategoriesList();

}
