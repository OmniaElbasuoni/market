package com.panorama.marketapp.Home;

import com.panorama.marketapp.Models.CateoriesResponse.CategoriesResponse;

import java.util.List;

public interface HomeView {
    void onOffersSliderResponse();
    void onProductsListResponse(List<CategoriesResponse> categoriesResponseList);
    void getErrorMessage(String msg, int code, Throwable t, Boolean isToast);
}
