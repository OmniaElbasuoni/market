package com.panorama.marketapp.Home;

import com.panorama.marketapp.Models.CateoriesResponse.CategoriesResponse;
import com.panorama.marketapp.Remote.ApiUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomePresenter implements HomeViewPresenter{


    private HomeView mView;

    HomePresenter(HomeView mView) {
        this.mView = mView;
    }

    @Override
    public void getOffersSlider() {
        mView.onOffersSliderResponse();

    }

    @Override
    public void getCategoriesList() {
        ApiUtils.getCategoriesNetwork().getCategories().enqueue(new Callback<List<CategoriesResponse>>() {
            @Override
            public void onResponse(Call<List<CategoriesResponse>> call, Response<List<CategoriesResponse>> response) {
                if(response.isSuccessful()){
                        mView.onProductsListResponse(response.body());
                }
                else {
                }
            }

            @Override
            public void onFailure(Call<List<CategoriesResponse>> call, Throwable t) {

            }
        });

    }
}
