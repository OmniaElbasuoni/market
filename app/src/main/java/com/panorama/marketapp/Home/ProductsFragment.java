package com.panorama.marketapp.Home;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.panorama.marketapp.Home.Adapters.ProductsAdapter;
import com.panorama.marketapp.Models.CateoriesResponse.ProductsItem;
import com.panorama.marketapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductsFragment extends Fragment {

    ArrayList<ProductsItem> productsItems;
    String Name,Image;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.categoryName)
    TextView categoryName;
    @BindView(R.id.categoryImage)
    ImageView categoryImage;
    @BindView(R.id.productssRec)
    RecyclerView productssRec;

    NavController navController;



    private List<ProductsItem> mProductList;
    ProductsAdapter productsAdapter;

    private GridLayoutManager layoutManagerProducts;




    public ProductsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        productsItems = getArguments().getParcelableArrayList("Products");
        Name=getArguments().getString("categoryName");
        Image=getArguments().getString("categoryImage");
       // Toast.makeText(getActivity(), productsItems.get(0).getName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_products, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
        setupUI();
    }

   private void setupUI()
    {
        categoryName.setText(Name);
        Picasso.get()
                .load(Image)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.no_image)
                .into(categoryImage);


        mProductList = new ArrayList<>();

        layoutManagerProducts = new GridLayoutManager(getActivity(), 2);
        productsAdapter = new ProductsAdapter(mProductList,navController);
        productssRec.setLayoutManager(layoutManagerProducts);
        productssRec.setAdapter(productsAdapter);


        if (productsItems.size() > 0) {
            mProductList.addAll(productsItems);
            productsAdapter.notifyDataSetChanged();
        }
    }



    @OnClick(R.id.back)
    public void onViewClicked() {
        navController.navigateUp();
    }
}
