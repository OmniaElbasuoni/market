package com.panorama.marketapp.Home.Adapters;

import android.content.Context;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.panorama.marketapp.Models.SliderOffers;
import com.panorama.marketapp.R;

import java.util.List;

public class OffersSliderAdapter extends PagerAdapter {

    private Context context;
    private List<SliderOffers> galleryList;
    private OnOfferClickListener mClickListener;

    public OffersSliderAdapter(Context context, List<SliderOffers> galleryList) {
        this.context = context;
        this.galleryList = galleryList;
    }

    @Override
    public int getCount() {
        return galleryList.size();
    }
    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view.equals(o);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup view, final int position) {

        View imageLayout = LayoutInflater.from(context).inflate(R.layout.offer_slider_row, view, false);
        assert imageLayout != null;
        final ImageView imageView =  imageLayout.findViewById(R.id.offerImage);
        /*final TextView textView = imageLayout.findViewById(R.id.offersNumber);
        textView.setText();
        Picasso.get().load(galleryList.get(position).getImage()).into(imageView) ;*/
        imageView.setImageResource(R.drawable.slider);

        view.addView(imageLayout, 0);
     /*   imageLayout.setOnClickListener(v -> {
            mClickListener.OnItemClick(position);
        });*/
        return imageLayout;
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    public void setOnClickListener(OnOfferClickListener clickListener){
        this.mClickListener = clickListener;
    }

    public interface OnOfferClickListener {
        void OnItemClick(int position);
    }
}
