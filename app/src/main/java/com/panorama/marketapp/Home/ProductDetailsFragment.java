package com.panorama.marketapp.Home;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.panorama.marketapp.R;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductDetailsFragment extends Fragment {


    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.categoryName)
    TextView categoryName;

    NavController navController;

    String productName;

    String productImage;
    @BindView(R.id.Image)
    ImageView Image;

    public ProductDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        productName = getArguments().getString("productName");
        productImage = getArguments().getString("productImage");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_product_details, container, false);
        ButterKnife.bind(this, view);
        categoryName.setText(productName);
        Picasso.get()
                .load(productImage)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.no_image)
                .into(Image);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);

    }

    @OnClick(R.id.back)
    public void onViewClicked() {
        navController.navigateUp();
    }
}
