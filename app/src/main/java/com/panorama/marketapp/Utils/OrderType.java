package com.panorama.marketapp.Utils;

import java.io.Serializable;

public enum OrderType implements Serializable {
    Wasalny,
    Akelny,
    Tsawqly
}
