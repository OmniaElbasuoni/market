package com.panorama.marketapp.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Environment;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.Spinner;

import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;

public class AppUtils {

    private static AppUtils Instance = null;
    private Context context;


    public static AppUtils getInstance() {
        if (Instance == null)
            Instance = new AppUtils();
        return Instance;
    }

    //handle request WRITE_EXTERNAL_STORAGE before use this method
    //To See It In Galley
    public void SaveImageInExternalStorage(Bitmap finalBitmap) {
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_images");
        if (!myDir.exists()) {
            myDir.mkdirs();
        }
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-"+ n +".jpg";
        File file = new File (myDir, fname);
        if (file.exists ())
            file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //using this line you will be able to see saved images in the gallery view.
        /*context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
                Uri.parse("file://" + Environment.getExternalStorageDirectory())));*/
    }

    public void snackBar(View parentLayout , String str){
        Snackbar snack = Snackbar.make(parentLayout, str, Snackbar.LENGTH_LONG);
        View view = snack.getView();
        FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)view.getLayoutParams();
        params.gravity = Gravity.BOTTOM;
        view.setLayoutParams(params);
        view.setBackgroundColor(Color.parseColor("#16b0e4"));
        snack.setDuration(2000);
        snack.show();
    }
    public void makeSpinner(ArrayAdapter arrayAdapter,
                                   Spinner spinner, AdapterView.OnItemSelectedListener onItemSelectedListener){
        //modelList = new ArrayList<>();
        //ArrayAdapter arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, carBrandsList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        arrayAdapter.notifyDataSetChanged();
        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(onItemSelectedListener);
    }

}
