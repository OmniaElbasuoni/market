package com.panorama.marketapp.Utils;

import android.app.Activity;
import android.widget.EditText;
import android.widget.Toast;

import com.panorama.marketapp.R;


public class Validator {


    /*global field validator for this app*/
    private static final String EMAIL_VERIFICATION = "^([\\w-\\.]+){1,64}@([\\w&&[^_]]+){2,255}.[a-z]{2,}$";


    /*this function validates a array of edit text */
    public static boolean validateInputField(EditText[] array,
                                             Activity context) {
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i].getText().toString().length() == 0) {
                Toast.makeText(context, array[i].getTag().toString() + context.getResources().getString(R.string.is_empty), Toast.LENGTH_LONG).show();
                break;
            } else {
                if ((array[i].getTag().toString().equals("Email"))) {
                    if (array[i].getText().toString().trim().matches(EMAIL_VERIFICATION)) {
                        count++;
                    } else {
                        array[i].setError(array[i].getTag().toString() + context.getResources().getString(R.string.is_invalid));
                        Toast.makeText(context, array[i].getTag().toString() + context.getResources().getString(R.string.is_invalid), Toast.LENGTH_SHORT).show();
                        break;
                    }
                } else {
                    count++;
                }
            }
        }


        return array.length == count;
    }



}